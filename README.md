# Angular Pokemon Trainer

A pokemon trainer app for adding pokemon to your trainer collection.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.2.0.

### Features:
* Login or register a new user
* Look through a catalogue of first generation Pokemon
* See the stats of the selected pokemon in a pokedex
* Catch the pokemon you want and add them to your trainer collection
* See your collection in your trainer profile

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

## Authors

Arvin Khodabandeh, Håkon Holm Erstad

### Attributions
Pokecomputer Login Page image: https://www.shutterstock.com/image-vector/mistical-pokemon-trainers-workplace-pokeball-pikachu-1545319814 \
Pokeball drawing style image: https://pixabay.com/illustrations/pokemon-pokeball-nintendo-gaming-1513925/ \
Poke ball image: https://www.freeiconspng.com/img/45330 \
Pokedex image: https://www.kindpng.com/imgv/hmmmxRm_original-pokedex-hd-png-download/ \
Pokemon Trainer icon created by Nikita Golubev: https://www.flaticon.com/free-icons/pokemon
