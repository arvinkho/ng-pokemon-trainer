export const environment = {
  production: true,
  pokeAPIURL: "https://pokeapi.co/api/v2/pokemon?limit=150",
  appAPIKey: "L6Vueqym9EysejQc7Ps5mA==",
  appAPIURL: "https://ak-noroff-api.herokuapp.com/trainers",
  trainerKey: "poke-trainer",
  pokeCatalogueKey: "poke-catalogue",
  pokeSpriteURL: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/"
};
