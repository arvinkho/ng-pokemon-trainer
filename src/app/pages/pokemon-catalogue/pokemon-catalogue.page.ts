import { Component, OnDestroy, OnInit } from "@angular/core";
import { NavigationStart, Router } from "@angular/router";
import { Pokemon } from "src/app/models/pokemon.model";
import { User } from "src/app/models/trainer.model";
import { PokemonCatalogueService } from "src/app/services/pokemon-catalogue.service";
import { UserService } from "src/app/services/user.service";
import { environment } from "src/environments/environment";

const TRAINER_STORAGE_KEY = environment.trainerKey;

@Component({
	selector: "app-pokemon-catalogue",
	templateUrl: "./pokemon-catalogue.page.html",
	styleUrls: ["./pokemon-catalogue.page.css"],
})
export class PokemonCataloguePage implements OnInit, OnDestroy {
	private routeSub: any;

	get pokemons(): Pokemon[] {
		return this.pokemonCatalogueService.pokemons;
	}

	get trainer(): User | undefined {
		return this.userService.user;
	}

	constructor(
		private router: Router,
		private pokemonCatalogueService: PokemonCatalogueService,
		private userService: UserService
	) {}

	ngOnInit(): void {
		if(localStorage.getItem(TRAINER_STORAGE_KEY)){
			this.pokemonCatalogueService.findAllPokemons();
		}

		this.routeSub = this.router.events.subscribe((event) => {
			if (event instanceof NavigationStart) {
				this.userService.updateApiCollection();
        		console.log("Navigating away from page");
			}
		});
	}

	onDisplayPokemonDetails(pokemon: Pokemon) {
		this.router.navigate(["/pokemon-catalogue", pokemon.id]);
	}

	public ngOnDestroy() {
		this.routeSub.unsubscribe();
	}
}
