import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { Router } from "@angular/router";
import { User } from "src/app/models/trainer.model";
import { UserService } from "src/app/services/user.service";
import { environment } from "src/environments/environment";

const TRAINER_STORAGE_KEY = environment.trainerKey;

@Component({
	selector: "app-login",
	templateUrl: "./login.page.html",
	styleUrls: ["./login.page.css"],
})
export class LoginPage implements OnInit {
	get user(): User | undefined {
		return this.userService.user;
	}

	constructor(private router: Router, private userService: UserService) {}

	ngOnInit(): void {
		if (this.user?.username) {
			this.router.navigateByUrl("/pokemon-catalogue");
		}
	}

	onLoginSubmit(form: NgForm): void {
		// Check the user
		const { username } = form.value;

		this.userService.findUser(username).subscribe({
			next: (response: boolean) => {
				if (response) {
					// Save to storage?
					localStorage.setItem(TRAINER_STORAGE_KEY, JSON.stringify(this.userService.user));

					// Redirect
					this.router.navigateByUrl("/pokemon-catalogue");
				} else {
					alert("User does not exist");
				}
			},
		});
	}
}
