import { Component, OnInit } from "@angular/core";
import { Pokemon } from "src/app/models/pokemon.model";
import { User } from "src/app/models/trainer.model";
import { PokemonCatalogueService } from "src/app/services/pokemon-catalogue.service";
import { UserService } from "src/app/services/user.service";
import { environment } from "src/environments/environment";

const TRAINER_STORAGE_KEY = environment.trainerKey;
const POKE_CATALOGUE_KEY = environment.pokeCatalogueKey;

@Component({
	selector: "app-trainer",
	templateUrl: "./trainer.page.html",
	styleUrls: ["./trainer.page.css"],
})
export class TrainerPage implements OnInit {
	get pokemons(): Pokemon[] {
		return this.pokemonCatalogueService.pokemons;
	}

	get trainer(): User | undefined {
		return this.userService.user;
	}

	constructor(private userService: UserService, private pokemonCatalogueService: PokemonCatalogueService) {}

	ngOnInit(): void {
		if (this.pokemonCatalogueService.pokemons.length === 0) {
			this.pokemonCatalogueService.findAllPokemons();
		}
	}
}
