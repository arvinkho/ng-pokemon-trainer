import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { Router } from "@angular/router";
import { User } from "src/app/models/trainer.model";
import { RegisterService } from "src/app/services/register.service";
import { UserService } from "src/app/services/user.service";
import { environment } from "src/environments/environment";

const TRAINER_STORAGE_KEY = environment.trainerKey;

@Component({
	selector: "app-register",
	templateUrl: "./register.page.html",
	styleUrls: ["./register.page.css"],
})
export class RegisterPage implements OnInit {
	private _userExists: boolean = true;

	constructor(private registerService: RegisterService, private router: Router, private userService: UserService) {}

	get user(): User | undefined {
		return this.userService.user;
	}

	get userExists(): boolean {
		return this._userExists;
	}

	set userExists(ifExist: boolean) {
		this._userExists = ifExist;
	}

	ngOnInit(): void {
		if (this.user?.username) {
			this.router.navigateByUrl("/pokemon-catalogue");
		}
	}

	onCheckUser(username: string) {
		this.userService.findUser(username).subscribe({
			next: (response: boolean) => {
				this._userExists = response;
				if (this._userExists) {
					alert("User already exists.");
				}
			},
		});
	}

	onRegisterSubmit(form: NgForm) {
		const { username } = form.value;
		this.registerService.createUser(username).subscribe({
			next: (response: boolean) => {
				if (response) {
					// Save to storage?
					localStorage.setItem(TRAINER_STORAGE_KEY, JSON.stringify(this.userService.user));

					// Redirect
					this._userExists = true;
					this.router.navigateByUrl("/pokemon-catalogue");
				} else {
					alert("User could not be created.");
				}
			},
		});
	}
}
