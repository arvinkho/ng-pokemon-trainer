import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CurrentStat, Pokemon, PokeResponse, PokeStats } from 'src/app/models/pokemon.model';
import { PokemonCatalogueService } from 'src/app/services/pokemon-catalogue.service';

@Component({
  selector: 'app-pokemon-detail',
  templateUrl: './pokemon-detail.page.html',
  styleUrls: ['./pokemon-detail.page.css']
})
export class PokemonDetailPage implements OnInit {

  private _pokemon?: Pokemon;
  
  get pokemon(): Pokemon | undefined {
    return this._pokemon;
  }

  get pokemons(): Pokemon[] | undefined {
    return this.pokemonCatalogueService.pokemons;
  }

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private pokemonCatalogueService: PokemonCatalogueService,
    private http: HttpClient
  ) { }

  ngOnInit(): void {
    const pokemonId: string = this.route.snapshot.paramMap.get("pokemonId") ?? "";
    console.log(pokemonId);
    
    this._pokemon = this.pokemons?.find(pokemon => pokemon.id === parseInt(pokemonId))
    if (this._pokemon === undefined) {
      this.router.navigateByUrl("/pokemon-catalogue")
    }
  }

}
