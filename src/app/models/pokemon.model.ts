export interface Pokemon {
    name: string;
    url: string;
    id: number;
    img: string;
    collected: boolean;
    stats?: PokeStats;
}

export interface PokeStats {
    hp?: number;
    attack?: number;
    defense?: number;
    specialattack?: number;
    specialdefence?: number;
    speed?: number;
    
}

export interface CurrentStat {
    base_stat?: number;
}


export interface PokeCollectionResponse<T> {
    count: number;
    next?: string;
    previous?: string;
    results: T;
}

export interface PokeResponse<T> {
    stats: T[]
}
