import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http'

import { AppComponent } from './app.component';
import { LoginPage } from './pages/login/login.page';
import { PokemonCataloguePage } from './pages/pokemon-catalogue/pokemon-catalogue.page';
import { TrainerPage } from './pages/trainer/trainer.page';
import { PokemonCatalogueListComponent } from './components/pokemon-catalogue-list/pokemon-catalogue-list.component';
import { PokemonCatalogueListItemComponent } from './components/pokemon-catalogue-list-item/pokemon-catalogue-list-item.component';
import { RegisterPage } from './pages/register/register.page';
import { PokemonDetailPage } from './pages/pokemon-detail/pokemon-detail.page';
import { TrainerCollectionListComponent } from './components/trainer-collection-list/trainer-collection-list.component';
import { TrainerCollectionListItemComponent } from './components/trainer-collection-list-item/trainer-collection-list-item.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginPage,
    PokemonCataloguePage,
    TrainerPage,
    PokemonCatalogueListComponent,
    PokemonCatalogueListItemComponent,
    RegisterPage,
    PokemonDetailPage,
    TrainerCollectionListComponent,
    TrainerCollectionListItemComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
