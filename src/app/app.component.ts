import { Component } from '@angular/core';
import { User } from './models/trainer.model';
import { UserService } from './services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ng-pokemon-trainer';

  get user(): User | undefined{
    return this.userService.user;
  }

  constructor(
    private userService: UserService
  ) { }

  onSignOutClick():void {
    this.userService.signOutUser();
  }
}
