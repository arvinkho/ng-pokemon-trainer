import { Injectable } from "@angular/core";
import { Pokemon } from "../models/pokemon.model";
import { PokemonCatalogueService } from "./pokemon-catalogue.service";

@Injectable({
	providedIn: "root",
})
export class PokemonDetailService {
	private _pokemon?: Pokemon;

	get pokemons(): Pokemon[] {
		return this.pokemonCatalogueService.pokemons;
	}

	get pokemon(): Pokemon | undefined {
		return this._pokemon;
	}

	constructor(private pokemonCatalogueService: PokemonCatalogueService) {}
}
