import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { map, Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { User } from "../models/trainer.model";
import { UserService } from "./user.service";

const URL = environment.appAPIURL;
const API_KEY = environment.appAPIKey;

@Injectable({
	providedIn: "root",
})
export class RegisterService {
	constructor(private http: HttpClient, private userService: UserService) {}

	private createHeaders(): HttpHeaders {
		return new HttpHeaders({
			"Content-Type": "application/json",
			"X-API-KEY": API_KEY,
		});
	}

	createUser(username: string): Observable<boolean> {
		const user = {
			username,
			pokemon: [],
		};
		const headers = this.createHeaders();
		return this.http.post<User>(URL, user, { headers }).pipe(
			map((response: User) => {
				if (!response.username) {
					console.log("Could not create user");
					return false;
				}
				this.userService.user = response;
				return true;
			})
		);
	}
}
