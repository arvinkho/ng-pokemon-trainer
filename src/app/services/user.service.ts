import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { map, Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { User } from "../models/trainer.model";

const URL = environment.appAPIURL;
const API_KEY = environment.appAPIKey;
const TRAINER_STORAGE_KEY = environment.trainerKey;
const POKE_CATALOGUE_KEY = environment.pokeCatalogueKey;

@Injectable({
	providedIn: "root",
})
export class UserService {
	private _user?: User;

	get user(): User | undefined {
		return this._user;
	}

	set user(user: User | undefined) {
		this._user = user;
	}

	private createHeaders(): HttpHeaders {
		return new HttpHeaders({
			"Content-Type": "application/json",
			"X-API-KEY": API_KEY,
		});
	}

	constructor(private http: HttpClient, private router: Router) {
		if (localStorage.getItem(TRAINER_STORAGE_KEY)) {
			const user = localStorage.getItem(TRAINER_STORAGE_KEY) || "";

			this._user = JSON.parse(user) || undefined;
		}
	}

	findUser(username: string): Observable<boolean> {
		this._user = undefined;

		return this.http.get<User[]>(`${URL}?username=${username}`).pipe(
			map((response: User[]) => {
				if (!response || response.length === 0) return false;
				this._user = response[0];

				if (!response[0].username) {
					console.log("Could not find user");
					return false;
				} else {
					return true;
				}
			})
		);
	}

	updateApiCollection() {
		const body = {
			pokemon: this._user?.pokemon,
		};
		const headers = this.createHeaders();

		this.http.patch(`${URL}/${this._user?.id}`, body, { headers }).subscribe(
			(val) => {
				console.log("PATCH call successful value returned in body", val);
			},
			(response) => {
				console.log("PATCH call in error", response);
			}
		);
	}

	addPokemonToCollection(pokemonName: string) {
		this._user?.pokemon.push(pokemonName);
		localStorage.setItem(TRAINER_STORAGE_KEY, JSON.stringify(this._user));
	}

	removePokemonFromCollection(pokemonName: string) {
		this._user?.pokemon.splice(this._user.pokemon.indexOf(pokemonName), 1);
		localStorage.setItem(TRAINER_STORAGE_KEY, JSON.stringify(this._user));
		this.updateApiCollection();
	}

	signOutUser(): void {
		console.log("signing out");
		this.updateApiCollection();
		this._user = undefined;
		sessionStorage.removeItem(POKE_CATALOGUE_KEY);
		localStorage.removeItem(TRAINER_STORAGE_KEY);
		this.router.navigateByUrl("/login");
	}
}
