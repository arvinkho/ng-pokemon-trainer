import { HttpClient, HttpResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { finalize, map } from "rxjs";
import { environment } from "src/environments/environment";
import { CurrentStat, PokeCollectionResponse, Pokemon, PokeResponse, PokeStats } from "../models/pokemon.model";
import { User } from "../models/trainer.model";

const POKE_CATALOGUE_KEY = environment.pokeCatalogueKey;
const URL = environment.pokeAPIURL;
const POKE_SPRITE_URL = environment.pokeSpriteURL;

@Injectable({
	providedIn: "root",
})
export class PokemonCatalogueService {
	private _pokemons: Pokemon[] = [];
	private _trainer!: User;
	private _loading: boolean = false;

	get pokemons(): Pokemon[] {
		return this._pokemons;
	}

	get trainer(): User {
		return this._trainer;
	}

	set pokemons(pokemon: Pokemon[]) {
		this._pokemons = pokemon;
	}

	constructor(private http: HttpClient) {}

	findAllPokemons(): void {
		const data = sessionStorage.getItem(POKE_CATALOGUE_KEY);
		if (data === null) {
			this.http
				.get<PokeCollectionResponse<Pokemon[]>>(URL)
				.pipe(
					map((response: PokeCollectionResponse<Pokemon[]>) =>
						response.results.map((pokemon) => {
							let currentId = parseInt(pokemon.url.split("/").slice(-2, -1).toString());
							return {
								...pokemon,
								name: pokemon.name[0].toUpperCase() + pokemon.name.substring(1),
								id: currentId,
								img: `${POKE_SPRITE_URL}${currentId}.png`,
							};
						})
					) // CONSIDER ERROR HANDLING HERE!!!
				)
				.subscribe({
					next: (pokemons: Pokemon[]) => {
						this._pokemons = pokemons;
						sessionStorage.setItem(POKE_CATALOGUE_KEY, JSON.stringify(this._pokemons));
					},
				});
		} else {
			this._pokemons = JSON.parse(data);
		}
	}

	getPokemonStats(currentPokemon: Pokemon): void {
		this._loading = true;
		this.http
			.get<PokeResponse<CurrentStat>>(currentPokemon.url)
			.pipe(
				map((response) => {
					return {
						...currentPokemon.stats,
						hp: response.stats[0].base_stat,
						attack: response.stats[1].base_stat,
						defense: response.stats[2].base_stat,
						specialattack: response.stats[3].base_stat,
						specialdefence: response.stats[4].base_stat,
						speed: response.stats[5].base_stat,
					};
				}),
				finalize(() => {
					this._loading = false;
				})
			)
			.subscribe({
				next: (stats: PokeStats) => {
					currentPokemon.stats = stats;
					let updatedPokemons = this._pokemons.filter((pokemon) => pokemon.name !== currentPokemon.name);
					this._pokemons = [...updatedPokemons, currentPokemon].sort((a, b) => {
						return a.id - b.id;
					});
					sessionStorage.setItem(POKE_CATALOGUE_KEY, JSON.stringify(this._pokemons));
				},
			});
	}
}
