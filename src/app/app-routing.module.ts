import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { LoginPage } from "./pages/login/login.page";
import { TrainerPage } from "./pages/trainer/trainer.page";
import { PokemonCataloguePage } from "./pages/pokemon-catalogue/pokemon-catalogue.page";
import { RegisterPage } from "./pages/register/register.page";
import { UserAuthenticationGuard } from "./guards/user-authentication.guard";
import { PokemonDetailPage } from "./pages/pokemon-detail/pokemon-detail.page";

const routes: Routes = [
    {
        path: "",
        redirectTo: "/login",
        pathMatch: "full"
    },
    {
        path: "login",
        component: LoginPage
    },
    {
        path: "register",
        component: RegisterPage
    },
    {
        path: "trainer",
        component: TrainerPage,
        canActivate: [ UserAuthenticationGuard ]
    },
    {
        path: "pokemon-catalogue",
        component: PokemonCataloguePage,
        canActivate: [ UserAuthenticationGuard ]
    },
    {
        path: "pokemon-catalogue/:pokemonId",
        component: PokemonDetailPage,
        canActivate: [ UserAuthenticationGuard ]
    }
]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {}