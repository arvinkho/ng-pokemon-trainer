import { Component, Input, OnInit } from "@angular/core";
import { Pokemon } from "src/app/models/pokemon.model";
import { User } from "src/app/models/trainer.model";

@Component({
	selector: "app-trainer-collection-list",
	templateUrl: "./trainer-collection-list.component.html",
	styleUrls: ["./trainer-collection-list.component.css"],
})
export class TrainerCollectionListComponent implements OnInit {
	@Input() pokemons: Pokemon[] = [];
	@Input() trainer: User | undefined;

	private _collectionPokemon: Pokemon[] = [];

	get collectionPokemon() {
		return this._collectionPokemon;
	}

	set collectionPokemon(pokemon: Pokemon[]) {
		this._collectionPokemon = pokemon;
	}

	constructor() {}

	ngOnInit(): void {
		this.setCollectedPokemon();
		this._collectionPokemon = this.findCollectionPokemon();
	}

	findCollectionPokemon(): Pokemon[] {
		const tempCollection = [];
		for (let i = 0; i < this.pokemons.length; i++) {
			if (this.pokemons[i].collected) {
				tempCollection.push(this.pokemons[i]);
			}
		}
		return tempCollection;
	}

	setCollectedPokemon() {
		for (let i = 0; i < this.pokemons.length; i++) {
			this.pokemons[i].collected = this.trainer?.pokemon.includes(this.pokemons[i].name.toLowerCase()) ?? false;
		}
	}
}
