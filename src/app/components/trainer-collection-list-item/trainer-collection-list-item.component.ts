import { Component, Input, OnInit } from "@angular/core";
import { Pokemon } from "src/app/models/pokemon.model";
import { User } from "src/app/models/trainer.model";
import { UserService } from "src/app/services/user.service";

@Component({
	selector: "app-trainer-collection-list-item",
	templateUrl: "./trainer-collection-list-item.component.html",
	styleUrls: ["./trainer-collection-list-item.component.css"],
})
export class TrainerCollectionListItemComponent implements OnInit {
	@Input() pokemon!: Pokemon;
	@Input() trainer: User | undefined;
	@Input() collected: boolean = true;

	constructor(private userService: UserService) {}

	ngOnInit(): void {}

	onRemovePokemonClick(): void {
		if (this.collected) {
			this.userService.removePokemonFromCollection(this.pokemon.name.toLowerCase());
			this.collected = false;
		}
	}
}
