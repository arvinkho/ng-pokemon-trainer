import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { Pokemon } from "src/app/models/pokemon.model";
import { User } from "src/app/models/trainer.model";

@Component({
	selector: "app-pokemon-catalogue-list",
	templateUrl: "./pokemon-catalogue-list.component.html",
	styleUrls: ["./pokemon-catalogue-list.component.css"],
})
export class PokemonCatalogueListComponent implements OnInit {
	@Input() pokemons: Pokemon[] = [];
	@Input() trainer: User | undefined;
	@Output() displayPokemonDetails: EventEmitter<Pokemon> = new EventEmitter();

	constructor() {}

	ngOnInit(): void {}

	onDisplayPokemonDetails(pokemon: Pokemon) {
		this.displayPokemonDetails.emit(pokemon);
	}
}
