import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { Pokemon } from "src/app/models/pokemon.model";
import { User } from "src/app/models/trainer.model";
import { PokemonCatalogueService } from "src/app/services/pokemon-catalogue.service";
import { UserService } from "src/app/services/user.service";

@Component({
	selector: "app-pokemon-catalogue-list-item",
	templateUrl: "./pokemon-catalogue-list-item.component.html",
	styleUrls: ["./pokemon-catalogue-list-item.component.css"],
})
export class PokemonCatalogueListItemComponent implements OnInit {
	@Input() pokemon!: Pokemon;
	@Input() collected: boolean = false;
	@Input() trainer: User | undefined;
	@Output() displayPokemonDetails: EventEmitter<Pokemon> = new EventEmitter();

	constructor(private userService: UserService, private pokemonCatalogueService: PokemonCatalogueService) {}

	ngOnInit(): void {
		this.collected = this.trainer?.pokemon.includes(this.pokemon.name.toLowerCase()) ?? false;
		this.pokemon.collected = this.trainer?.pokemon.includes(this.pokemon.name.toLowerCase()) ?? false;
	}

	onCollectPokemonClick(): void {
		if (!this.collected) {
			this.userService.addPokemonToCollection(this.pokemon.name.toLowerCase());
			this.collected = true;
			this.pokemon.collected = this.collected;
		}
	}

	onPokedexClick(): void {
		this.pokemonCatalogueService.getPokemonStats(this.pokemon);
		this.displayPokemonDetails.emit(this.pokemon);
	}
}
